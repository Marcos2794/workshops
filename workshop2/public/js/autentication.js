const jwt = require("jsonwebtoken");

// JWT Authentication
app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      const authToken = req.headers['authorization'].split(' ')[1];
      try {
        jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
          if (err || !decodedToken) {
            res.status(401);
            res.json({
              error: "Unauthorized "
            });
          }
          console.log('decodedToken', decodedToken);
  
          if (decodedToken.userId == 123) {
            next();
          }
        });
      } catch (e) {
        next();
      }
  
    } else {
      res.status(401);
      res.send({
        error: "Unauthorized "
      });
    }
  });