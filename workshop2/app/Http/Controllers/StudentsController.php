<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::all()->toArray();
        return response()->json($student);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
    		$student = new Student([
    			'firstname'=>$request->input('firstname'),
                'lastname'=>$request->input('lastname'),
    			'email'=>$request->input('email'),
                'address'=>$request->input('address'),
    			]);
    		$student->save();
    		return response()->json(['status'=>true, 'Muchas gracias'], 200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido añadir: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
    		$student = Student::find($id);
    		if(!$student){
    			return response()->json(['No existe...'], 404);
    		}
    		
    		return response()->json($student, 200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido encontrar el estudiante: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
    		$student = Student::find($id);
    		if(!$student){
    			return response()->json(['No existe...'], 404);
    		}
    		
    		$student->update($request->all());
    		return response(array(
                'error' => false,
                'message' =>'Alumno Modificado...',
               ),200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido editar: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
    		$student = Student::find($id);
    		if(!$student){
    			return response()->json(['No existe...'], 404);
    		}
    		
    		$student->delete();
    		return response()->json('Estudiante eliminado..', 200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido eliminar: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }
}
